<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*CAMBIAR MAIN POR HOME, para que se vea "mejor"*/
Route::get('/', function () {
    if(Auth::check()){
		return view('/main'); //cambia main por welcome para ver lo de login
	}else{
		return view('auth/login');
	}
});

/*
Route::get('/master', function () {//esto hay que cambiarlo
	if(Auth::check()){
		return view('/main');
	}else{
		return view('auth/login');
	}
    
});
*/
Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/main', 'HomeController@index');

//usuarios
Route::resource('/usuarios','UsersController');
Route::get('/usuarios/delete/{id?}','UsersController@delete');



//pacientes
Route::resource('/pacientes','PacientesController');
Route::get('/pacientes/delete/{id?}','PacientesController@delete');

//enfermedades
Route::resource('/enfermedades','EnfermedadesController');
Route::get('/enfermedades/delete/{id?}','EnfermedadesController@delete');


