<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\Traits\EntrustUserTrait;
//Eloquent
//Authenticatable
class User extends Authenticatable
{
    use SoftDeletes, EntrustUserTrait {

        SoftDeletes::restore insteadof EntrustUserTrait;
        EntrustUserTrait::restore insteadof SoftDeletes;

    }
    //use EntrustUserTrait; // add this trait to your user model
    use Notifiable;
    //use SoftDeletes;

    protected $table = "users";
    //public $timestamps = true;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //un usuario puede ser de un tipo de usuario
    public function tipo_usuario()
    {
        return $this->hasOne('App\TipoUsuario','id','tipo_usuario_id');
    }
}
