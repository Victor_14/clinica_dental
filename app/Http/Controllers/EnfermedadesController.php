<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use DB;

use App\Enfermedad as Enfermedad;

class EnfermedadesController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $enfermedades = Enfermedad::all();

        return view('enfermedades.index')->with('enfermedades',$enfermedades);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->can('crear_enfermedades')){
            return view('enfermedades.add');
        }else{
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el  :attribute',
            'unique' => '¡El :attribute ya existe!',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
        ];
        //validador de los input del formulario
        $validator = Validator::make($request->all(), [
                'nombre'  => 'required|max:180',
            ], $messages);

        //Si contiene errores se devuelve al formulario con todos los errores, de lo contrario guarda en la base de datos
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }else{
            $enfermedad = new Enfermedad();
            $enfermedad->nombre = formato_guarda_input($request->input("nombre"));
            $enfermedad->config = 1;
            $enfermedad->save();
        }

        return redirect("enfermedades")->with('success', 'add')->with("id_enfermedad", $enfermedad->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->can('ver_enfermedades')){
            $enfermedad = Enfermedad::findorfail($id);
            return view('enfermedades.modalver')->with('enfermedad',$enfermedad);
        }else{
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->can('editar_enfermedades')){
            $enfermedad = Enfermedad::findorfail($id);
            return view('enfermedades.edit')->with('enfermedad',$enfermedad);
        }else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el  :attribute',
            'unique' => '¡El :attribute ya existe!',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
        ];
        //validador de los input del formulario
        $validator = Validator::make($request->all(), [
                'nombre'  => 'required|max:180',
            ], $messages);

        //Si contiene errores se devuelve al formulario con todos los errores, de lo contrario guarda en la base de datos
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }else{
            $enfermedad = Enfermedad::findorfail($id);
            $enfermedad->nombre = formato_guarda_input($request->input("nombre"));
            
            $enfermedad->save();
        }

        return redirect("enfermedades")->with('success', 'edit')->with("id_enfermedad", $enfermedad->id);
    }

    /*para modal que pregunta si quiere borrar*/
    public function delete($id=null)
    {
        if(Auth::user()->can('eliminar_enfermedades')){
            $enfermedad = Enfermedad::findorfail($id);
            if($enfermedad == NULL){
                return redirect('pacientes');
            }else{
                return view('enfermedades.modaldelete')->with('enfermedad',$enfermedad);
            }
        }else{
            return redirect()->back();
        }  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $enfermedad = Enfermedad::findorfail($id);
        $enfermedad->delete();

        return redirect("enfermedades")->with('success','delete');
    }
}
