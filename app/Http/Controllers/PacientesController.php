<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; /*para poder usar el Auth:: ...*/

use Validator;
use DB;

use App\Paciente as Paciente;
use App\Enfermedad as Enfermedad;
use App\EnfermedadesFicha as EnfermedadesFicha;
use App\Ficha as Ficha;
use App\User as User;

class PacientesController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pacientes = Paciente::all();

        return view('pacientes.index')->with('pacientes',$pacientes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->can('crear_pacientes')){
            $enfermedades = Enfermedad::all();
            return view('pacientes.add')->with('enfermedades',$enfermedades);
        }else{
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        //dd($request->all());
        //mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el  :attribute',
            'correo.required'    => 'Debe ingresar el  correo',
            'numeric' => 'El :attribute debe solo contener números',
            'unique' => '¡El :attribute ya existe!',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'confirmed' => 'Debe ingresar las 2 contraseñas iguales',
            'email' => 'Debe ingresar un correo valido',
            'cl_rut' =>'Debe ingresar un rut valido',
            'date' => ':attribute no es una fecha válida,ingrese una fecha válida.',
        ];
        //validador de los input del formulario
        $validator = Validator::make($request->all(), [
                'nombre'  => 'required|max:100',
                'apellido' => 'required|max:100',
                'rut'   => 'required|cl_rut',
                'edad' => 'required|numeric',
                'fecha_nacimiento' => 'required|date',
                'prevision' => 'required|',
                'correo' => 'required|email|unique:pacientes,correo|max:255',
                'telefono_contacto' => 'numeric',
                'direccion' => 'required',
            ], $messages);

        //Si contiene errores se devuelve al formulario con todos los errores, de lo contrario guarda en la base de datos
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }else{
            $paciente = new Paciente();
            $paciente->nombre = formato_guarda_input($request->input("nombre"));
            $paciente->apellido = formato_guarda_input($request->input("apellido"));
            $paciente->correo = formato_guarda_input($request->input("correo"));
            if($request->input("telefono_contacto") != ""){
                $paciente->telefono_contacto = $request->input("telefono_contacto");
            }else{
                $paciente->telefono_contacto = NULL;
            }
            
            $paciente->edad = $request->input("edad");
            $paciente->fecha_nacimiento = formato_fecha($request->input("fecha_nacimiento"));
            $paciente->direccion = formato_guarda_input($request->input("direccion"));
            $paciente->prevision = formato_guarda_input($request->input("prevision"));
            $paciente->rut = formato_guarda_rut($request->input("rut"));
            /*FILTRO PARA QUE NO SE DUPLIQUEN LOS RUT*/
            $contador= DB::table('pacientes')->where('rut','=', $paciente->rut)->count();
            if($contador != 0){
                return redirect()->back()->withInput($request->all())->with('success', 'errorRut');
            }
            /*FIN FILTRO PARA QUE NO SE DUPLIQUEN LOS RUT*/
           
            
            $paciente->config = 1;
            $paciente->save();
            //crear ficha
            $nuevaFicha = new Ficha();
            $nuevaFicha->id_paciente = $paciente->id;
            $nuevaFicha->id_creador = Auth::user()->id;
            $nuevaFicha->motivo = formato_guarda_input($request->input("motivo"));
            $nuevaFicha->save();
            //crear enfermedadesFicha
            foreach ($request->input("enf") as $key => $enfermedad) {//$enfermedad tiene el id de las enfermedades seleccionadas
                //$aux =$aux."+".$enfermedad."+";
                $nuevaEnfermedadesFicha = new EnfermedadesFicha();
                $nuevaEnfermedadesFicha->id_ficha = $nuevaFicha->id;
                $nuevaEnfermedadesFicha->id_enfermedad = $enfermedad;
                $nuevaEnfermedadesFicha->save();
            }

            
        }

        return redirect("pacientes")->with('success', 'add')->with("id_paciente", $paciente->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->can('ver_pacientes')){
            $paciente = Paciente::findorfail($id);
            //obtener la edad del paciente
            $datetime1 = date_create($paciente->fecha_nacimiento);
            $datetime2 = date_create(date("d-m-Y"));
            $interval = date_diff($datetime1, $datetime2);
            //$añosDiferencia = $interval->y;
            $edadPaciente = $interval->y;
            return view('pacientes.modalver')->with('paciente',$paciente)->with('edadPaciente',$edadPaciente);
        }else{
            return redirect()->back();
        }

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->can('editar_pacientes')){
            $paciente = Paciente::findorfail($id);
            return view('pacientes.edit')->with('paciente',$paciente);
        }else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $paciente = Paciente::findorfail($id);
        //mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el  :attribute',
            'correo.required'    => 'Debe ingresar el  correo',
            'numeric' => 'El :attribute debe solo contener números',
            'unique' => '¡El :attribute ya existe!',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'confirmed' => 'Debe ingresar las 2 contraseñas iguales',
            'email' => 'Debe ingresar un correo valido',
            'cl_rut' =>'Debe ingresar un rut valido',
            'date' => ':attribute no es una fecha válida,ingrese una fecha válida.',
        ];

        if($paciente->correo == formato_guarda_input($request->input("correo"))){//no se cambia correo
            $validator = Validator::make($request->all(), [
                    'nombre'  => 'required|max:100',
                    'apellido' => 'required|max:100',
                    'rut'   => 'required|cl_rut',
                    'edad' => 'required|numeric',
                    'fecha_nacimiento' => 'required|date',
                    'prevision' => 'required|',
                    'correo' => 'required|email|max:255',
                    'telefono_contacto' => 'numeric',
                    'direccion' => 'required',
                ], $messages);
        }else{//se cambia correo
            //validador de los input del formulario
            $validator = Validator::make($request->all(), [
                    'nombre'  => 'required|max:100',
                    'apellido' => 'required|max:100',
                    'rut'   => 'required|cl_rut',
                    'edad' => 'required|numeric',
                    'fecha_nacimiento' => 'required|date',
                    'prevision' => 'required|',
                    'correo' => 'required|email|unique:pacientes,correo|max:255',
                    'telefono_contacto' => 'numeric',
                    'direccion' => 'required',
                ], $messages);
        }
        

        //Si contiene errores se devuelve al formulario con todos los errores, de lo contrario guarda en la base de datos
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }else{
            $paciente->nombre = formato_guarda_input($request->input("nombre"));
            $paciente->apellido = formato_guarda_input($request->input("apellido"));
            $paciente->correo = formato_guarda_input($request->input("correo"));
            if($request->input("telefono_contacto") != ""){
                $paciente->telefono_contacto = $request->input("telefono_contacto");
            }else{
                $paciente->telefono_contacto = NULL;
            }
            
            $paciente->edad = $request->input("edad");
            $paciente->fecha_nacimiento = formato_fecha($request->input("fecha_nacimiento"));
            $paciente->direccion = formato_guarda_input($request->input("direccion"));
            $paciente->prevision = formato_guarda_input($request->input("prevision"));

            if($paciente->rut != formato_guarda_rut($request->input("rut"))){//significa que se cambio el rut
                /*FILTRO PARA QUE NO SE DUPLIQUEN LOS RUT*/
                $contador= DB::table('pacientes')->where('rut','=', formato_guarda_rut($request->input("rut")))->count();
                if($contador != 0){
                    return redirect()->back()->withInput($request->all())->with('success', 'errorRut');
                }
                /*FIN FILTRO PARA QUE NO SE DUPLIQUEN LOS RUT*/
            }
            
            $paciente->rut = formato_guarda_rut($request->input("rut"));
            
            
            $paciente->save();
            //dd($request->all(),$paciente); 
        }

        return redirect("pacientes")->with('success', 'edit')->with("id_paciente", $paciente->id);
        
    }

    /*para modal que pregunta si quiere borrar*/
    public function delete($id=null)
    {
        if(Auth::user()->can('editar_pacientes')){
            $paciente = Paciente::findorfail($id);
            if($paciente == NULL){
                return redirect('pacientes');
            }else{
                return view('pacientes.modaldelete')->with('paciente',$paciente);
            }
        }else{
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd($id);
        $paciente = Paciente::findorfail($id);
        $paciente->delete();

        return redirect("pacientes")->with('success','delete');
    }
}
