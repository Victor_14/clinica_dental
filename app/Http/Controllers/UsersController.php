<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

use Validator;
use DB;

use App\User as Usuario;
use App\TipoUsuario as TipoUsuario;

class UsersController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = Usuario::all();
        
        return view('usuarios.index')->with('usuarios',$usuarios) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->can('crear_usuarios')){
            $tipos = TipoUsuario::all();
            return view('usuarios.add')->with('tipos',$tipos);
        }else{
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el  :attribute',
            'email.required'    => 'Debe ingresar el  correo',
            'numeric' => 'El :attribute debe solo contener números',
            'unique' => '¡El :attribute ya existe!',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'confirmed' => 'Debe ingresar las 2 contraseñas iguales',
            'email' => 'Debe ingresar un correo valido',
            'cl_rut' =>'Debe ingresar un rut valido',
        ];
        //validador de los input del formulario
        $validator = Validator::make($request->all(), [
                'nombre'  => 'required|max:100',
                'apellido' => 'required|max:100',
                //'apellido_materno' => 'required|max:255',
                'rut'   => 'required|cl_rut',
                'correo' => 'required|email|unique:users,email|max:255',
                'tipo_usuario' => 'required',
                'telefono_contacto' => 'numeric',
            ], $messages);

        //Si contiene errores se devuelve al formulario con todos los errores, de lo contrario guarda en la base de datos
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }else{
            $user = new Usuario();
            $user->nombre = formato_guarda_input($request->input("nombre"));
            $user->apellido = formato_guarda_input($request->input("apellido"));
            $user->email = formato_guarda_input($request->input("correo"));
            $user->telefono_contacto = $request->input("telefono_contacto");
            $user->rut = formato_guarda_rut($request->input("rut"));
            /*FILTRO PARA QUE NO SE DUPLIQUEN LOS RUT*/
            $contador= DB::table('users')->where('rut','=', $user->rut)->count();
            if($contador != 0){
                return redirect()->back()->withInput($request->all())->with('success', 'errorRut');
            }
            /*FIN FILTRO PARA QUE NO SE DUPLIQUEN LOS RUT*/
            $user->tipo_usuario_id = $request->input("tipo_usuario");
            if($user->tipo_usuario_id == 2 || $user->tipo_usuario_id == 3){
                if($request->input("porcentaje") != ""){
                    $user->porcentaje = $request->input("porcentaje");
                }else{
                    return redirect()->back()->withInput($request->all())->with('success', 'errorPorcentaje');
                }
            }else{
                $user->porcentaje = NULL;
            }
            $user->password = bcrypt($user->rut);
            $user->config = 1;
            $user->save();
            //dd($request->all(),$user);  
        }

        
        return redirect("usuarios")->with('success', 'add')->with("id_usuario", $user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->can('ver_usuarios')){
            $usuario = Usuario::findorfail($id);
            return view('usuarios.modalver')->with('usuario',$usuario);
        }else{
            return redirect()->back();
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dd(Auth::user()->can('editar_usuarios'));
        if(Auth::user()->can('editar_usuarios')){
            $tipos = TipoUsuario::all();
            $usuario = Usuario::findorfail($id);
            return view('usuarios.edit')->with('tipos',$tipos)->with('usuario',$usuario);
        }else{
            return redirect()->back();
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Usuario::findorfail($id);

        //mensajes de los validadores
        $messages = [
            'required'    => 'Debe ingresar el  :attribute',
            'email.required'    => 'Debe ingresar el  correo',
            'numeric' => 'El :attribute debe solo contener números',
            'unique' => '¡El :attribute ya existe!',
            'max' => 'El :attribute no debe exeder los :max caracteres',
            'min' => 'El :attribute debe tener minimo :min caracteres',
            'confirmed' => 'Debe ingresar las 2 contraseñas iguales',
            'email' => 'Debe ingresar un correo valido',
            'cl_rut' =>'Debe ingresar un rut valido',
        ];

        if($user->email == formato_guarda_input($request->input("correo")) ){//significa que no se cambio el mail
            //validador de los input del formulario
            $validator = Validator::make($request->all(), [
                'nombre'  => 'required|max:100',
                'apellido' => 'required|max:100',
                //'apellido_materno' => 'required|max:255',
                'rut'   => 'required|cl_rut',
                'correo' => 'required|email|max:255',
                'tipo_usuario' => 'required',
                'telefono_contacto' => 'numeric',
            ], $messages);
        }else{//significa que si se cambio el mail

            $validator = Validator::make($request->all(), [
                'nombre'  => 'required|max:100',
                'apellido' => 'required|max:100',
                //'apellido_materno' => 'required|max:255',
                'rut'   => 'required|cl_rut',
                'correo' => 'required|email|unique:users,email|max:255',
                'tipo_usuario' => 'required',
                'telefono_contacto' => 'numeric',
                ], $messages);

        }


        if($user->rut != formato_guarda_rut($request->input("rut"))){//significa que si se cambio el rut
            /*FILTRO PARA QUE NO SE DUPLIQUEN LOS RUT*/
            $contador= DB::table('users')->where('rut','=', formato_guarda_rut($request->input("rut")))->count();
            if($contador != 0){
                return redirect()->back()->withInput($request->all())->with('success', 'errorRut');
            }
            /*FIN FILTRO PARA QUE NO SE DUPLIQUEN LOS RUT*/
        }

        //Si contiene errores se devuelve al formulario con todos los errores, de lo contrario guarda en la base de datos
        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }else{
            
            $user->nombre = formato_guarda_input($request->input("nombre"));
            $user->apellido = formato_guarda_input($request->input("apellido"));
            $user->email = formato_guarda_input($request->input("correo"));
            $user->rut = formato_guarda_rut($request->input("rut"));
            $user->telefono_contacto = $request->input("telefono_contacto");
            $user->tipo_usuario_id = $request->input("tipo_usuario");
            if($user->tipo_usuario_id == 2 || $user->tipo_usuario_id == 3){
                if($request->input("porcentaje") != ""){
                    $user->porcentaje = $request->input("porcentaje");
                }else{
                    return redirect()->back()->withInput($request->all())->with('success', 'errorPorcentaje');
                }
            }else{
                $user->porcentaje = NULL;
            }
            $user->save();
              
        }

        return redirect("usuarios")->with('success', 'edit')->with("id_usuario", $user->id);
    }

    /*para modal que pregunta si quiere borrar*/
    public function delete($id=null)
    {
        if(Auth::user()->can('eliminar_usuarios')){
            $usuario = Usuario::findorfail($id);
            if($usuario == NULL){
                return redirect('usuarios');
            }else{
                return view('usuarios.modaldelete')->with('usuario',$usuario);
            }
        }else{
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd($id);
        $usuario = Usuario::findorfail($id);
        $usuario->delete();

        return redirect("usuarios")->with('success','delete');
    }
}
