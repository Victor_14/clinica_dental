<?php

function formato_rut($rut_param)
{ 
    $parte4 = substr($rut_param, -1); // seria solo el numero verificador 
    $parte3 = substr($rut_param, -4,3); // la cuenta va de derecha a izq  
    $parte2 = substr($rut_param, -7,3);  
    $parte1 = substr($rut_param, 0,-7); //de esta manera toma todos los caracteres desde el 8 hacia la izq 

    return $parte1.".".$parte2.".".$parte3."-".$parte4; 
}

function formato_guarda_rut($rut_param)
{
	/*formato rut ,para guardar en la base de datos, se guarda sin puntos ni guion y se guarda K*/
    $rut="";
    for ($i=0; $i<strlen($rut_param); $i++) {
        if (is_numeric($rut_param[$i]))
        {
            $rut.=$rut_param[$i];
        }
        if($i == (strlen($rut_param)-1) && $rut_param[$i] == "K")
        {

            $rut.=$rut_param[$i];
        }
    }
    /*fin formato rut*/
    return $rut;
}

function formato_guarda_input($input_param)
{
    /*
    $string1 = str_replace("ñ", "Ñ", $input_param);
    $string2 = str_replace("á", "Á", $string1);
    $string3 = str_replace("é", "É", $string2);
    $string4 = str_replace("í", "Í", $string3);
    $string5 = str_replace("ó", "Ó", $string4);
    $string6 = str_replace("ú", "Ú", $string5);
    */
    
    $string6 = mb_strtoupper($input_param,'UTF-8');
    $string7 = str_replace("Ê", "Ú", $string6);
	return strtoupper(trim($string7));
}

function formato_fecha($fecha="")
{
	if($fecha != "")
	{
		$particiones = explode("-", $fecha);
		$fecha = $particiones[2]."-".$particiones[1]."-".$particiones[0];
	}
	
	return $fecha;
}





?>