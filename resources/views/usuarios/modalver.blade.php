<div id="modalVer" class="modal fade" tabindex="-1" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header" style="color: rgb(41, 175, 228);">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<strong>Información del Usuario</strong>
			</div>
			<div class="modal-body">
						<table class="table table-bordered table-hover table-responsive">
						<tbody>
							<tr>
								<th width="25%">Nombre:</th>
								<td width="45%"> {{$usuario->nombre}} {{$usuario->apellido}}</td>
							</tr>
								<th >Correo:</th>
								<td >{{$usuario->email}}</td>
							<tr>
							</tr>
							<!--
							<tr>
								<th>Teléfono Fijo:</th>
								<td>{{$usuario->fono}}</td>
								<th>Teléfono Móvil:</th>
								<td>{{$usuario->movil}}</td>
							</tr>
							-->
							<tr>
							</tr>
							<!--
							<tr>
								<th>Departamento:</th>
								<td>{{$usuario->departamento}}</td>
								<th>Cargo:</th>
								<td>{{$usuario->cargo}}</td>
							</tr>
							-->
							<tr>
								<th>Tipo de Usuario:</th>
								<td>{{$usuario->tipo_usuario->nombre}}</td>
							</tr>
							@role(['ADMINISTRADOR','ADMINISTRADOR ODONTÓLOGO'])
							<tr>
								<th>Porcentaje:</th>
								<td>{{ $usuario->porcentaje }} %</td>
							</tr>
							@endrole
							<tr>
								<th>Telefono Contacto :</th>
								<td>{{ $usuario->telefono_contacto }}</td>
							</tr>
						</tbody>
					</table>
					
			</div><!--/modal body-->
			<div class="modal-footer">
				@permission('editar_usuarios')
				<a href="{{ url("/usuarios/$usuario->id/edit") }}" title="Editar" class="btn btn-warning pull-right"><i class="fa fa-edit"></i> Editar</a>
				@endpermission
			</div>
		</div>
	</div>
</div>
