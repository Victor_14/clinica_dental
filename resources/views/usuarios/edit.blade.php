@extends('master')

@section('title', 'Usuarios Editar')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/bootstrap-toastr/toastr.min.css') }}"/>

<!-- END PAGE LEVEL STYLES -->
@endpush

@section('content')

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-users" style="font-size: 23px !important; color: black"></i>Panel de Usuarios
							</div>
						</div>
						<div class="portlet-body form">
						@if ($errors->any())
							<div class="note note-danger">
								<h6 class="block"><strong>Sigue las siguientes instrucciones :</strong></h6>
								<ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
							</div>
						@endif
						@if(session("success"))
								@if(Session::get("success")=="errorRut")
									<br>
									<div class="alert alert-success alert-dismissable">
										<button type="button" title="Cerrar" class="close" data-dismiss="alert" aria-hidden="true"></button>
										<i class="fa fa-warning"></i> <strong>El rut ingresado ya pertenece a un usuario.</strong>
									</div>
								@endif
							@endif
						<form method="POST" class="horizontal-form" id="form_usuarios" action="{{ route('usuarios.update',$usuario->id) }}">
							<!--
							<input name="_method" type="hidden" value="PUT">-->
							{{ method_field('PUT') }}
							<div class="form-body">
								<h3 class="form-section">Editar usuario {{ $usuario->nombre." ".$usuario->apellido }}</h3>
								<div class="row">
									<div class="col-md-3">
										<div class="form-group{{ $errors->has('tipo_usuario') ? ' has-error' : '' }}">
											<label class="control-label">Tipo de Usuario (*)</label>
											<select id="tipo_usuario" name="tipo_usuario" class="form-control select2me" data-placeholder="Select..." required onchange="mostrar(this.value);">
												<option value="">Elegir Tipo de Usuario</option>
												@foreach($tipos as $tipo)
													@if($tipo->id == $usuario->tipo_usuario_id)
						                                <option value="{{$tipo->id }}" selected>{{ $tipo->nombre }}</option>
						                            @else
						                                <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>
						                            @endif
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-3">
										<div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
											<label class="control-label">Nombre (*)</label>
											<input type="text" id="nombre" name="nombre" class="form-control mayusculas letras" placeholder="Ej: Pedro" value="{{ $usuario->nombre }}" maxlength="100" required>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
											<label class="control-label">Apellido (*)</label>
											<input type="text" id="apellido" name="apellido" class="form-control mayusculas letras"  placeholder="Apellido Paterno del Usuario" maxlength="100" value="{{ $usuario->apellido }}" required>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group{{ $errors->has('rut') ? ' has-error' : '' }}">
											<label class="control-label">Rut (*)</label>
											<input type="text" id="rut" name="rut" class="form-control mayusculas ruts" placeholder="Rut del nuevo usuario" value="{{ formato_rut($usuario->rut) }}" maxlength="12" required onblur="return Rut(form_usuarios.rut.value)">
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group{{ $errors->has('correo') ? ' has-error' : '' }}">
											<label class="control-label">Correo (**)</label>
											<input type="email" id="correo" name="correo" class="form-control mayusculas"  value="{{ $usuario->email }}" placeholder="Ej: ejemplo@gmail.com" maxlength="255" required>
											<span class="help-block" style="font-size: 11px;">
	                                            Para pacientes no es obligación.
	                                        </span>
										</div>
									</div>
								</div><!--/row-->
								<div class="row">
									<div class="col-md-3">
										<div class="form-group{{ $errors->has('telefono_contacto') ? ' has-error' : '' }}">
											<label class="control-label">Teléfono Contacto</label>
											<input type="text" id="telefono_contacto" name="telefono_contacto" class="form-control mayusculas numeros"  value="{{$usuario->telefono_contacto }}" placeholder="Teléfono Celular" onkeyup="verificarFonoContacto(this.value);" maxlength="9" minlength="8">
											<span class="help-block" style="font-size: 11px;">
	                                            Número de Celular.
	                                        </span>
										</div>
									</div>
										<div class="col-md-2" id="paraOdontologos" style="display: none;">
											<div class="form-group{{ $errors->has('porcentaje') ? ' has-error' : '' }}">
												<label class="control-label">Porcentaje (*)</label>
												<div class="input-icon right">
													<i class="fa fa-percent"></i>
												</div>
												<input type="text" id="porcentaje" name="porcentaje" class="form-control mayusculas odontologoInputs numeros"  value="{{ $usuario->porcentaje }}" onkeyup="verificarPorcentaje(this.value);" placeholder="0" >
												<span class="help-block" style="font-size: 11px;">
		                                            Porcentaje para el odontólogo.
		                                        </span>
											</div>
										</div>
								</div> <!--/row-->
								
								
								
							</div><!--/div class="form-body"-->
							<div class="form-actions right">
								<a href="{{ asset('usuarios') }}" title="Cancelar" class="btn btn-default">Cancelar</a>
								<button type="submit" title="Guardar" class="btn btn-info"><i class="fa fa-check"></i> Guardar</button>
							</div>
							{{ csrf_field() }}
						</form>
						</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
<script src="{{ asset('assets/plugins/bootstrap-toastr/toastr.min.js') }}"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>
<script src="{{ asset('assets/scripts/ui-toastr.js') }}"></script>



<script type="text/javascript">
	$(document).ready(function(){
		App.init(); // initlayout and core plugins
		mostrar({{ $usuario->tipo_usuario_id }});
		verificarPorcentaje({{ $usuario->porcentaje }});

   		$("#usuarios").addClass( "active" );
		$("#usuarios-li").addClass( "active" );
		$("#usuarios-a").append( '<span class="selected"></span>' );

		/*Con esto no se deja ingresar nada que no sea letras*/
	    $(".letras").keypress(function (key) {
            window.console.log(key.charCode);//imprime el codigo ascii
            if (
                (key.charCode < 97 || key.charCode > 122)//letras mayusculas
                && (key.charCode < 65 || key.charCode > 90) //letras minusculas
                && (key.charCode != 0) //borrar
                && (key.charCode != 32) //espacio
                && (key.charCode != 241) //ñ
                 && (key.charCode != 209) //Ñ
                 && (key.charCode != 225) //á
                 && (key.charCode != 233) //é
                 && (key.charCode != 237) //í
                 && (key.charCode != 243) //ó
                 && (key.charCode != 250) //ú
                 && (key.charCode != 193) //Á
                 && (key.charCode != 201) //É
                 && (key.charCode != 205) //Í
                 && (key.charCode != 211) //Ó
                 && (key.charCode != 218) //Ú
                )
                //console.log(key.charCode);
                return false;
	    });
	    /*fin letras*/ 

	    /*Con esto no se deja ingresar nada que no sea rut*/
	    $(".ruts").keypress(function (key) {
	            //window.console.log(key.charCode)
	            if (
	                (key.charCode < 48 || key.charCode > 57)//números
	                && (key.charCode != 0) //borrar y enter
	                && (key.charCode != 107) //K
	                )
	                //console.log(key.charCode);
	                return false;
	    });
	    /*fin rut*/

	    /*Con esto no se deja ingresar nada que no sea numeros*/
	    $(".numeros").keypress(function (key) {
	            //window.console.log(key.charCode)
	            if (
	                (key.charCode < 48 || key.charCode > 57)//números
	                && (key.charCode != 0) //borrar y enter
	                )
	                //console.log(key.charCode);
	                return false;
	    });
	    /*fin numeros*/
	});

 options = {
      "closeButton": true,
      "debug": true,
      "positionClass": "toast-top-right",
      "showDuration": "1000",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }

/*FUNCIONES PARA RUT*/
function revisarDigito(dvr){    
  dv = dvr + ""    
  if( dv != '0' && dv != '1' && dv != '2' && dv != '3' && dv != '4' && dv != '5' && dv != '6' && dv != '7' && dv != '8' && dv != '9' && dv != 'k'  && dv != 'K'){
       
    //alert("Debe ingresar un digito verificador valido");      
    //document.form1.rut.focus();        
    //document.form1.rut.select();        
    return false;    
  }    
  return true;
}

function revisarDigito2(crut){    
  largo = crut.length;    
  if(largo<2){
            
    //alert("Debe ingresar el rut completo")        
    //document.form1.rut.focus();        
    //document.form1.rut.select();        
    return false;    
  }    
  if(largo>2)        
    rut = crut.substring(0, largo - 1);    
  else        
    rut = crut.charAt(0);    
    dv = crut.charAt(largo-1);    
    revisarDigito( dv );    

  if ( rut == null || dv == null )
    return 0    
    var dvr = '0'    
    suma = 0    
    mul  = 2    

    for (i= rut.length -1 ; i >= 0; i--){    
        suma = suma + rut.charAt(i) * mul        
        if (mul == 7)            
            mul = 2        
        else                
            mul++    
    }    
    res = suma % 11    
    if (res==1)        
        dvr = 'k'    
    else if (res==0)        
        dvr = '0'    
    else    
    {        
        dvi = 11-res        
        dvr = dvi + ""    
    }
    if ( dvr != dv.toLowerCase() )    
    {
        
        //alert("EL rut es incorrecto")        
        //document.form1.rut.focus();        
        //document.form1.rut.select();        
        return false    
    }

    return true
}

function Rut(texto){
	//alert(texto);    
  var tmpstr = "";    
  for ( i=0; i < texto.length ; i++ )        
    if ( texto.charAt(i) != ' ' && texto.charAt(i) != '.' && texto.charAt(i) != '-' )
        tmpstr = tmpstr + texto.charAt(i);    
    texto = tmpstr;    
    largo = texto.length;    

    if ( largo < 2 ){
    	/*
        $('#mensajeRut1').removeAttr("hidden");
        $('#mensajeRut2').attr("hidden","hidden");
        */       
        //alert("Debe ingresar el rut completo");
        //document.form1.rut.focus();        
        //document.form1.rut.select();        
        return false;    
    }    

    for (i=0; i < largo ; i++ ){            
        if ( texto.charAt(i) !="0" && texto.charAt(i) != "1" && texto.charAt(i) !="2" && texto.charAt(i) != "3" && texto.charAt(i) != "4" && texto.charAt(i) !="5" && texto.charAt(i) != "6" && texto.charAt(i) != "7" && texto.charAt(i) !="8" && texto.charAt(i) != "9" && texto.charAt(i) !="k" && texto.charAt(i) != "K" ){ 
                     
            //alert("El valor ingresado no corresponde a un rut valido");            
            //document.form1.rut.focus();            
            //document.form1.rut.select();            
            return false;        
        }    
    }    

    var invertido = "";    
    for ( i=(largo-1),j=0; i>=0; i--,j++ )        
        invertido = invertido + texto.charAt(i);    
    var dtexto = "";    
    dtexto = dtexto + invertido.charAt(0);    
    dtexto = dtexto + '-';    
    cnt = 0;    

    for ( i=1,j=2; i<largo; i++,j++ ){        
        //alert("i=[" + i + "] j=[" + j +"]" );        
        if ( cnt == 3 ){            
            dtexto = dtexto + '.';            
            j++;            
            dtexto = dtexto + invertido.charAt(i);            
            cnt = 1;        
        }else{                
           dtexto = dtexto + invertido.charAt(i);            
           cnt++;        
        }    
    }    

    invertido = "";    
    for ( i=(dtexto.length-1),j=0; i>=0; i--,j++ )        
        invertido = invertido + dtexto.charAt(i);    

    //alert(invertido);
    $("#rut").val(invertido.toUpperCase());
    //document.form_usuarios.rut.value = invertido.toUpperCase();      

    if(revisarDigito2(texto)){
        //ACÁ ESTA TODO BUENO, si se llega hasta acá
        
        return true; 
    }else{
        return false;
    }   
     
}
/*fin funciones para rut*/

/*inicio muestra segun tipo de usuario*/
function mostrar(id)
{
	/*cambiar por switch*/
	if(id==2 || id==3){//adminOdonto y odontologo
		$('#paraOdontologos').css("display","");
		$(".odontologoInputs").attr("required","required");
	}else{
		$('#paraOdontologos').css("display","none");
		$(".odontologoInputs").removeAttr("required","required");
	}

}
/*fin muestra segun tipo de usuario*/

/*inicio verificar porcentaje odontologo*/
function verificarPorcentaje(valor)
{
	if(valor > 100){
		$('#porcentaje').val("100");
		toastr.info("El porcentaje no puede ser mayor al 100%", "Ayuda", options);
	}
	if(valor == ""){
		$('#porcentaje').val("0");
	}
	return false;
}
/*fin verificar porcentaje odontologo*/

/*inicio verificar porcentaje odontologo*/
function verificarFonoContacto(valor)
{
	//alert(valor.length);
	if(valor.length >= 9){
		//alert(valor.length);
		toastr.info("El número no puede superar los 9 digitos", "Ayuda", options);
	}
	return false;
}
/*fin verificar porcentaje odontologo*/

</script>

@endpush