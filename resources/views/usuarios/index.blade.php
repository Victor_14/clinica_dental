@extends('master')

@section('title', 'Usuarios')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->
@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-users" style="font-size: 23px !important; color: black"></i>Panel de Usuarios
							</div>
							<div class="actions">
								<div class="btn-group">
									<a class="btn btn-info dropdown-toggle" href="#" data-toggle="dropdown">
									Columnas <i class="fa fa-angle-down"></i>
									</a>
									<div id="sample_2_column_toggler" class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
										<label><input type="checkbox" checked data-column="0">Nombre</label>
										<label><input type="checkbox" checked data-column="1">Correo</label>
										<label><input type="checkbox" checked data-column="2">Rut</label>
										<label><input type="checkbox" checked data-column="3">Tipo</label>
										<label><input type="checkbox" checked data-column="4">Acción</label>
									</div>
								</div>
							</div>
						</div>
						<div class="portlet-body">
							@if(session("success"))
								@if(Session::get("success")=="add")
									<div class="alert alert-success alert-dismissable">
										<button type="button" title="Cerrar" class="close" data-dismiss="alert" aria-hidden="true"></button>
										<i class="fa fa-thumbs-up fa-flip-horizontal"></i> <strong>¡Usuario creado correctamente!</strong> Tienes {{count($usuarios)}} usuarios en el sistema. <u><strong><a class="view-data" data-role="{{session('id_usuario')}}" style="cursor: pointer;" >Has click aqui para ver el último usuario que creaste</a></strong></u>
									</div>
								@endif
								@if(Session::get("success")=="edit")
									<div class="alert alert-success alert-dismissable">
										<button type="button" title="Cerrar" class="close" data-dismiss="alert" aria-hidden="true"></button>
										<i class="fa fa-thumbs-up fa-flip-horizontal"></i> <strong>¡Usuario editado correctamente!</strong> Tienes {{count($usuarios)}} usuarios en el sistema. <u><strong><a class="view-data" data-role="{{session('id_usuario')}}" style="cursor: pointer;" >Has click aqui para ver el último usuario que editaste</a></strong></u>
									</div>
								@endif
								@if(Session::get("success")=="delete")
									<div class="alert alert-success alert-dismissable">
										<button type="button" title="Cerrar" class="close" data-dismiss="alert" aria-hidden="true"></button>
										<i class="fa fa-thumbs-up fa-flip-horizontal"></i> <strong>¡Usuario eliminado correctamente!</strong> Tienes {{count($usuarios)}} usuarios en el sistema.
									</div>
								@endif
							@endif
							<div class="table-toolbar">
								<div class="btn-group">
									@permission('crear_usuarios')
									<a id="sample_editable_1_new" title="Agregar Nuevo Usuario" href="usuarios/create" class="btn btn-success">
									Agregar Nuevo Usuario <i class="fa fa-plus"></i>
									</a>
									@endpermission
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
								<thead>
								<tr>
									<th>Nombre</th>
									<th>Correo</th>
									<th class="hidden-xs">Rut</th>
									<th class="hidden-xs">Tipo</th>
									<th class="hidden-xs">Acción</th>
									
								</tr>
								</thead>
								<tbody>
								@foreach($usuarios as $usuario)
								<tr>
									<td>{{ $usuario->nombre." ".$usuario->apellido }}</td>
									<td>{{ $usuario->email }}</td>
									<td>{{ formato_rut($usuario->rut) }}</td>
									<td>{{ $usuario->tipo_usuario->nombre}}</td>
									<td>
										<!-- div botones-->
										<div class="clearfix">
											<div class="btn-group">
												@permission('ver_usuarios')
													<a class="btn btn-default view-data" title="Ver" data-role="{{$usuario->id}}" ><i class="fa fa-eye"></i></a>
												@endpermission
												
												@if($usuario->config == 1 && Auth::user()->id != $usuario->id)
													@permission('editar_usuarios')
														<a href="{{ url("/usuarios/$usuario->id/edit") }}" title="Editar" class="btn btn-default"><i class="fa fa-edit"></i></a>
													@endpermission
													@permission('eliminar_usuarios')
														<a class="btn btn-default delete-data" title="Eliminar" data-role="{{$usuario->id}}" ><i class="fa fa-trash"></i></a>
													@endpermission
												@endif
											</div>
										</div>
										<!-- /div botones -->
									</td>
								</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>
<script src="{{ asset('assets/scripts/table-advanced.js') }} "></script>


<script type="text/javascript">
	$(document).ready(function(){
		App.init(); // initlayout and core plugins
		TableAdvanced.init();

   		$("#usuarios").addClass( "active" );
		$("#usuarios-li").addClass( "active" );
		$("#usuarios-a").append( '<span class="selected"></span>' );

		$(".delete-data").click(function(){
		  var data = $(this).data("role");
		  $.get( "usuarios/delete/" + data, function( data ) {
			  $( "#modal" ).html( data );
			  $( "#modalEliminar" ).modal();
			});
		});

		$(".view-data").click(function(){
		  var data = $(this).data("role");
		  $.get( "usuarios/" + data, function( data ) {
			  $( "#modal" ).html( data );
			  $( "#modalVer" ).modal();
			});
		});

	});
</script>

@endpush