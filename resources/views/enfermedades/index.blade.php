@extends('master')

@section('title', 'Enfermedades')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/plugins/data-tables/DT_bootstrap.css')}}"/>
<!-- END PAGE LEVEL STYLES -->

@endpush

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="flaticon-002-first-aid-kit flaticon-med" style="color: black;"></i>Panel de Enfermedades
							</div>
							<div class="actions">
								<div class="btn-group">
									<a class="btn btn-info dropdown-toggle" href="#" data-toggle="dropdown">
									Columnas <i class="fa fa-angle-down"></i>
									</a>
									<div id="sample_2_column_toggler" class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
										<label><input type="checkbox" checked data-column="0">Nombre</label>
										<label><input type="checkbox" checked data-column="1">Acción</label>
									</div>
								</div>
							</div>
						</div>
						<div class="portlet-body">
							@if(session("success"))
								@if(Session::get("success")=="add")
									<div class="alert alert-success alert-dismissable">
										<button type="button" title="Cerrar" class="close" data-dismiss="alert" aria-hidden="true"></button>
										<i class="fa fa-thumbs-up fa-flip-horizontal"></i> <strong>Enfermedad creada correctamente!</strong> Tienes {{count($enfermedades)}} enfermedades en el sistema. <u><strong><a class="view-data" data-role="{{session('id_enfermedad')}}" style="cursor: pointer;" >Has click aqui para ver la última enfermedad que creaste</a></strong></u>
									</div>
								@endif
								@if(Session::get("success")=="edit")
									<div class="alert alert-success alert-dismissable">
										<button type="button" title="Cerrar" class="close" data-dismiss="alert" aria-hidden="true"></button>
										<i class="fa fa-thumbs-up fa-flip-horizontal"></i> <strong>Enfermedad editada correctamente!</strong> Tienes {{count($enfermedades)}} enfermedades en el sistema. <u><strong><a class="view-data" data-role="{{session('id_enfermedad')}}" style="cursor: pointer;" >Has click aqui para ver la última enfermedad que editaste</a></strong></u>
									</div>
								@endif
								@if(Session::get("success")=="delete")
									<div class="alert alert-success alert-dismissable">
										<button type="button" title="Cerrar" class="close" data-dismiss="alert" aria-hidden="true"></button>
										<i class="fa fa-thumbs-up fa-flip-horizontal"></i> <strong>Enfermedad eliminada correctamente!</strong> Tienes {{count($enfermedades)}} enfermedades en el sistema.
									</div>
								@endif
							@endif
							<div class="table-toolbar">
								<div class="btn-group">
									@permission('crear_enfermedades')
									<a id="sample_editable_1_new" title="Agregar Nueva enfermedad" href="enfermedades/create" class="btn btn-success">
									Agregar Nueva enfermedad <i class="fa fa-plus"></i>
									</a>
									@endpermission
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
								<thead>
								<tr>
									<th>Nombre</th>
									<th class="hidden-xs">Acción</th>
								</tr>
								</thead>
								<tbody>
								@foreach($enfermedades as $enfermedad)
								<tr>
									<td>{{ $enfermedad->nombre }}</td>
									<td>
										<!-- div botones-->
										<div class="clearfix">
											<div class="btn-group">
												@permission('ver_enfermedades')
												<a class="btn btn-default view-data" title="Ver" data-role="{{$enfermedad->id}}" ><i class="fa fa-eye"></i></a>
												@endpermission
												@if($enfermedad->config == 1)
												@permission('editar_enfermedades')
												<a href="{{ url("/enfermedades/$enfermedad->id/edit") }}" title="Editar" class="btn btn-default"><i class="fa fa-edit"></i></a>
												@endpermission
												@permission('eliminar_enfermedades')
												<a class="btn btn-default delete-data" title="Eliminar" data-role="{{$enfermedad->id}}" ><i class="fa fa-trash"></i></a>
												@endpermission
												@endif
											</div>
										</div>
										<!-- /div botones -->
									</td>
								</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
			
			
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
<div id="modal"></div>
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/data-tables/DT_bootstrap.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>
<script src="{{ asset('assets/scripts/table-advanced.js') }} "></script>


<script type="text/javascript">
	$(document).ready(function(){
		App.init(); // initlayout and core plugins
		TableAdvanced.init();

   		$("#enfermedades").addClass( "active" );
		$("#enfermedades-li").addClass( "active" );
		$("#enfermedades-a").append( '<span class="selected"></span>' );

		$(".delete-data").click(function(){
		  var data = $(this).data("role");
		  $.get( "enfermedades/delete/" + data, function( data ) {
			  $( "#modal" ).html( data );
			  $( "#modalEliminar" ).modal();
			});
		});

		$(".view-data").click(function(){
		  var data = $(this).data("role");
		  $.get( "enfermedades/" + data, function( data ) {
			  $( "#modal" ).html( data );
			  $( "#modalVer" ).modal();
			});
		});

	});
</script>

@endpush