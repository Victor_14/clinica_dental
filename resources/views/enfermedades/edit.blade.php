@extends('master')

@section('title', 'Enfermedades Editar')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/bootstrap-toastr/toastr.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>


<!-- END PAGE LEVEL STYLES -->
@endpush

@section('content')

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="flaticon-002-first-aid-kit flaticon-med" style="color: black;"></i>Panel de Enfermedades
							</div>
						</div>
						<div class="portlet-body form">
						@if ($errors->any())
							<div class="note note-danger">
								<h6 class="block"><strong>Sigue las siguientes instrucciones :</strong></h6>
								<ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
							</div>
						@endif
						<form method="post" class="horizontal-form" id="form_enfermedades" action="{{ route('enfermedades.update',$enfermedad->id) }}">
						{{ method_field('PUT') }}
							<div class="form-body">
								<h3 class="form-section">Editar Enfermedad</h3>
								<div class="row">
									<div class="col-md-8">
										<div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
											<label class="control-label">Nombre (*)</label>
											<input type="text" id="nombre" name="nombre" class="form-control mayusculas" value="{{ $enfermedad->nombre }}" placeholder="Nombre de la Enfermedad" maxlength="180" required onkeyup="verificarNombre(this.value);">
										</div>
									</div>
								</div><!--/row-->
								
								
								
								
								
							</div><!--/div class="form-body"-->
							<div class="form-actions right">
								<a href="{{ asset('enfermedades') }}" title="Cancelar" class="btn btn-default">Cancelar</a>
								<button type="submit" title="Guardar" class="btn btn-info"><i class="fa fa-check"></i> Guardar</button>
							</div>
							{{ csrf_field() }}
						</form>
						</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
<script src="{{ asset('assets/plugins/bootstrap-toastr/toastr.min.js') }}"></script>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>
<script src="{{ asset('assets/scripts/ui-toastr.js') }}"></script>



<script type="text/javascript">
	$(document).ready(function(){
		App.init(); // initlayout and core plugins
		

   		$("#enfermedades").addClass( "active" );
		$("#enfermedades-li").addClass( "active" );
		$("#enfermedades-a").append( '<span class="selected"></span>' );
		

		/*
	    $("#fecha_nacimiento").change(function(){
            alert($('#fecha_nacimiento').val());
            //$('#valor2').val($(this).val());
		});
		+/

		/*Con esto no se deja ingresar nada que no sea letras*/
	    $(".letras").keypress(function (key) {
            //window.console.log(key.charCode);//imprime el codigo ascii
            if (
                (key.charCode < 97 || key.charCode > 122)//letras mayusculas
                && (key.charCode < 65 || key.charCode > 90) //letras minusculas
                && (key.charCode != 0) //borrar
                && (key.charCode != 32) //espacio
                && (key.charCode != 241) //ñ
                 && (key.charCode != 209) //Ñ
                 && (key.charCode != 225) //á
                 && (key.charCode != 233) //é
                 && (key.charCode != 237) //í
                 && (key.charCode != 243) //ó
                 && (key.charCode != 250) //ú
                 && (key.charCode != 193) //Á
                 && (key.charCode != 201) //É
                 && (key.charCode != 205) //Í
                 && (key.charCode != 211) //Ó
                 && (key.charCode != 218) //Ú
                )
                //console.log(key.charCode);
                return false;
	    });
	    /*fin letras*/

	    

	});

 options = {
      "closeButton": true,
      "debug": true,
      "positionClass": "toast-top-right",
      "showDuration": "1000",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }


/*inicio verificar nombre*/
function verificarNombre(valor)
{
	//alert(valor.length);
	if(valor.length >= 180){
		//alert(valor.length);
		toastr.info("El nombre no puede superar los 180 caracteres", "Ayuda", options);
	}
	return false;
}
/*fin verificar nombre*/

</script>

@endpush