<div id="modalVer" class="modal fade" tabindex="-1" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header" style="color: rgb(41, 175, 228);">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<strong>Información de la Enfermedad</strong>
			</div>
			<div class="modal-body">
						<table class="table table-bordered table-hover table-responsive">
						<tbody>
							<tr>
								<th width="25%">Nombre:</th>
								<td width="45%"> {{$enfermedad->nombre}}</td>
							</tr>
						</tbody>
					</table>
					
			</div><!--/modal body-->
			<div class="modal-footer">
				@permission('editar_enfermedades')
				<a href="{{ url("/enfermedades/$enfermedad->id/edit") }}" title="Editar" class="btn btn-warning pull-right"><i class="fa fa-edit"></i> Editar</a>
				@endpermission
			</div>
		</div>
	</div>
</div>
