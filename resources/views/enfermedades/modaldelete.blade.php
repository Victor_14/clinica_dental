<div id="modalEliminar" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-md" >
        <div class="modal-content">
          <div class="modal-header" style="color: red">
           <i class="fa fa-warning"></i> <strong>  ELIMINAR </strong>
          </div>
          <div class="modal-body">
          	<center>
			<p>¿Está seguro que desea borrar esta enfermedad?</p>
			<p>{{ $enfermedad->nombre }}</p>
			</center>
			<form action="{{ route('enfermedades.destroy',$enfermedad->id) }}" method="post">
				<div class="modal-footer">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<span>
				    	<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					</span>
					<span>
						<button type="submit" title="Confirmar" class="btn btn-info btn-borrar">Confirmar</a>
					</span>
                	{{ method_field('DELETE') }}
				</div>
			</form>
        	</div>
    	</div>
  	</div><!--/modal dialog-->
</div>
