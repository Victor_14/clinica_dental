<div id="modalVer" class="modal fade" tabindex="-1" aria-hidden="false">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header" style="color: rgb(41, 175, 228);">
				<button type="button" title="Cerrar" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<strong>Información del Paciente</strong>
			</div>
			<div class="modal-body">
						<table class="table table-bordered table-hover table-responsive">
						<tbody>
							<tr>
								<th width="25%">Nombre:</th>
								<td width="45%"> {{$paciente->nombre}} {{$paciente->apellido}}</td>
							</tr>
							<tr>
								<th >Correo:</th>
								<td >{{$paciente->correo}}</td>
							</tr>
							<tr>
								<th>Edad:</th>
								<td>{{$edadPaciente}}</td>
							</tr>
							<tr>
								<th>Fecha Nacimiento:</th>
								<td>{{ formato_fecha($paciente->fecha_nacimiento) }}</td>
							</tr>
							<tr>
								<th>Rut:</th>
								<td>{{ formato_rut($paciente->rut) }}</td>
							</tr>
							<tr>
								<th>Teléfono Contacto:</th>
								<td>{{ $paciente->telefono_contacto }}</td>
								
							</tr>
							<tr>
								<th>Dirección :</th>
								<td>{{$paciente->direccion }}</td>
							</tr>
							<tr>
								<th>Previsión:</th>
								<td>{{$paciente->prevision }}</td>
							</tr>
							
						</tbody>
					</table>
					
			</div><!--/modal body-->
			<div class="modal-footer">
				@permission('editar_pacientes')
				<a href="{{ url("/pacientes/$paciente->id/edit") }}" title="Editar" class="btn btn-warning pull-right"><i class="fa fa-edit"></i> Editar</a>
				@endpermission
			</div>
		</div>
	</div>
</div>
