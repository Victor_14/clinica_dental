@extends('master')

@section('title', 'Pacientes Editar')

@push('css-head')
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/select2/select2_conquer.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/bootstrap-toastr/toastr.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker.css')}}"/>


<!-- END PAGE LEVEL STYLES -->
@endpush

@section('content')

@section('content')

<!-- BEGIN CONTENT -->

<div class="page-content-wrapper">
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="flaticon-015-chair flaticon-med" style="color: black;"></i>Panel de Pacientes
							</div>
						</div>
						<div class="portlet-body form">
						@if ($errors->any())
							<div class="note note-danger">
								<h6 class="block"><strong>Sigue las siguientes instrucciones :</strong></h6>
								<ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
							</div>
						@endif
						@if(session("success"))
								@if(Session::get("success")=="errorRut")
									<br>
									<div class="alert alert-success alert-dismissable">
										<button type="button" title="Cerrar" class="close" data-dismiss="alert" aria-hidden="true"></button>
										<i class="fa fa-warning"></i> <strong>El rut ingresado ya pertenece a un paciente.</strong>
									</div>
								@endif
							@endif
						<form method="post" class="horizontal-form" id="form_pacientes" action="{{ route('pacientes.update',$paciente->id) }}">
							{{ method_field('PUT') }}
							<div class="form-body">
								<h3 class="form-section">Editar Paciente</h3>
								<div class="row">
									<div class="col-md-3">
										<div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
											<label class="control-label">Nombre (*)</label>
											<input type="text" id="nombre" name="nombre" class="form-control mayusculas letras" value="{{ $paciente->nombre }}" placeholder="Nombre del paciente" maxlength="100" required>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
											<label class="control-label">Apellido (*)</label>
											<input type="text" id="apellido" name="apellido" class="form-control mayusculas letras" value="{{ $paciente->apellido }}" placeholder="Apellido del paciente" maxlength="100" required>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group{{ $errors->has('rut') ? ' has-error' : '' }}">
											<label class="control-label">Rut (*)</label>
											<input type="text" id="rut" name="rut" class="form-control mayusculas ruts"  value="{{ formato_rut($paciente->rut) }}" placeholder="Rut del paciente" maxlength="12" required onblur="return Rut(form_pacientes.rut.value)">
											<span class="help-block" style="font-size: 11px;">
	                                            Sin puntos ni guion.
	                                        </span>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group{{ $errors->has('edad') ? ' has-error' : '' }}">
											<label class="control-label">Edad (*)</label>
											<input type="text" id="edad" name="edad" class="form-control mayusculas numeros"  value="{{ $paciente->edad }}" maxlength="3" minlength="1" placeholder="Edad al momento de registrar" required>
										</div>
									</div>
								</div><!--/row-->
								<div class="row">
									<div class="col-md-3">
										<div class="form-group{{ $errors->has('fecha_nacimiento') ? ' has-error' : '' }}">
											<label class="control-label">Fecha de Nacimiento (*)</label>
											<input id="fecha_nacimiento" name="fecha_nacimiento" class="form-control"  value="{{ formato_fecha($paciente->fecha_nacimiento) }}" required>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group{{ $errors->has('prevision') ? ' has-error' : '' }}">
											<label class="control-label">Previsión (*)</label>
											<input type="text" id="prevision" name="prevision" class="form-control mayusculas"  value="{{ $paciente->prevision }}" maxlength="100" minlength="2" placeholder="Ej: fonasa" required>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group{{ $errors->has('correo') ? ' has-error' : '' }}">
											<label class="control-label">Correo (*)</label>
											<input type="email" id="correo" name="correo" class="form-control mayusculas"  value="{{ $paciente->correo }}" maxlength="255" placeholder="Ej: ejemplo@gmail.com" required>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group{{ $errors->has('telefono_contacto') ? ' has-error' : '' }}">
											<label class="control-label">Teléfono Contacto</label>
											<input type="text" id="telefono_contacto" name="telefono_contacto" class="form-control mayusculas numeros"  value="{{ $paciente->telefono_contacto }}" placeholder="Teléfono Celular" onkeyup="verificarFonoContacto(this.value);" maxlength="9" minlength="8">
											<span class="help-block" style="font-size: 11px;">
	                                            Número de Celular.
	                                        </span>
										</div>
									</div>
									
								</div><!--/row-->
								<div class="row">
									<div class="col-md-6">
										<div class="form-group{{ $errors->has('direccion') ? ' has-error' : '' }}">
											<label class="control-label">Dirección (*)</label>
											<input type="text" id="direccion" name="direccion" class="form-control mayusculas"  value="{{ $paciente->direccion }}" maxlength="255" minlength="1" placeholder="Ej: Huelen #3488 Cerro Navia" required>
										</div>
									</div>
								</div><!--/row-->
								<h3 class="form-section">Agregar datos para la ficha</h3>
								
								
								
							</div><!--/div class="form-body"-->
							<div class="form-actions right">
								<a href="{{ asset('pacientes') }}" title="Cancelar" class="btn btn-default">Cancelar</a>
								<button type="submit" title="Guardar" class="btn btn-info"><i class="fa fa-check"></i> Guardar</button>
							</div>
							{{ csrf_field() }}
						</form>
						</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PORTLET-->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Date Pickers
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse"></a>
								<a href="#portlet-config" data-toggle="modal" class="config"></a>
								<a href="javascript:;" class="reload"></a>
								<a href="javascript:;" class="remove"></a>
							</div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="#" class="form-horizontal form-bordered">
								<div class="form-body">
									
									
									<div class="form-group">
										<label class="control-label col-md-3">Months Only</label>
										<div class="col-md-3">
											<div class="input-group input-medium date date-picker" data-date="10/2012" data-date-format="mm/yyyy" data-date-viewmode="years" data-date-minviewmode="months">
												<input type="text" class="form-control" readonly>
												<span class="input-group-btn">
													<button class="btn btn-info" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<!-- /input-group -->
											<span class="help-block">
												 Select month only
											</span>
										</div>
									</div>
									
							
												
												
											</form>
										</div>
										
									</div>
								</div>
							</div>
							<!-- END FORM-->
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
			</div>
		</div> <!--/div class="page-content" -->
	</div> <!--/div class="page-content-wrapper" -->
</div><!--/div class="page-content-wrapper" -->
<!-- END CONTENT -->
@endsection

@push('script-footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/select2/select2.min.js') }} "></script>
<script src="{{ asset('assets/plugins/bootstrap-toastr/toastr.min.js') }}"></script>

<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('assets/scripts/app.js') }} "></script>
<script src="{{ asset('assets/scripts/ui-toastr.js') }}"></script>



<script type="text/javascript">
	$(document).ready(function(){
		App.init(); // initlayout and core plugins
		

   		$("#pacientes").addClass( "active" );
		$("#pacientes-li").addClass( "active" );
		$("#pacientes-a").append( '<span class="selected"></span>' );
		$("#fecha_nacimiento").datepicker({
	    format: 'dd-mm-yyyy',
	    language: 'es',
	    });

		/*
	    $("#fecha_nacimiento").change(function(){
            alert($('#fecha_nacimiento').val());
            //$('#valor2').val($(this).val());
		});
		+/

		/*Con esto no se deja ingresar nada que no sea letras*/
	    $(".letras").keypress(function (key) {
            //window.console.log(key.charCode);//imprime el codigo ascii
            if (
                (key.charCode < 97 || key.charCode > 122)//letras mayusculas
                && (key.charCode < 65 || key.charCode > 90) //letras minusculas
                && (key.charCode != 0) //borrar
                && (key.charCode != 32) //espacio
                && (key.charCode != 241) //ñ
                 && (key.charCode != 209) //Ñ
                 && (key.charCode != 225) //á
                 && (key.charCode != 233) //é
                 && (key.charCode != 237) //í
                 && (key.charCode != 243) //ó
                 && (key.charCode != 250) //ú
                 && (key.charCode != 193) //Á
                 && (key.charCode != 201) //É
                 && (key.charCode != 205) //Í
                 && (key.charCode != 211) //Ó
                 && (key.charCode != 218) //Ú
                )
                //console.log(key.charCode);
                return false;
	    });
	    /*fin letras*/

	    /*Con esto no se deja ingresar nada que no sea rut*/
	    $(".ruts").keypress(function (key) {
	            //window.console.log(key.charCode)
	            if (
	                (key.charCode < 48 || key.charCode > 57)//números
	                && (key.charCode != 0) //borrar y enter
	                && (key.charCode != 107) //K
	                )
	                //console.log(key.charCode);
	                return false;
	    });
	    /*fin rut*/

	    /*Con esto no se deja ingresar nada que no sea numeros*/
	    $(".numeros").keypress(function (key) {
	            //window.console.log(key.charCode)
	            if (
	                (key.charCode < 48 || key.charCode > 57)//números
	                && (key.charCode != 0) //borrar y enter
	                )
	                //console.log(key.charCode);
	                return false;
	    });
	    /*fin numeros*/
	});

 options = {
      "closeButton": true,
      "debug": true,
      "positionClass": "toast-top-right",
      "showDuration": "1000",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }

/*FUNCIONES PARA RUT*/
function revisarDigito(dvr){    
  dv = dvr + ""    
  if( dv != '0' && dv != '1' && dv != '2' && dv != '3' && dv != '4' && dv != '5' && dv != '6' && dv != '7' && dv != '8' && dv != '9' && dv != 'k'  && dv != 'K'){
       
    //alert("Debe ingresar un digito verificador valido");      
    //document.form1.rut.focus();        
    //document.form1.rut.select();        
    return false;    
  }    
  return true;
}

function revisarDigito2(crut){    
  largo = crut.length;    
  if(largo<2){
            
    //alert("Debe ingresar el rut completo")        
    //document.form1.rut.focus();        
    //document.form1.rut.select();        
    return false;    
  }    
  if(largo>2)        
    rut = crut.substring(0, largo - 1);    
  else        
    rut = crut.charAt(0);    
    dv = crut.charAt(largo-1);    
    revisarDigito( dv );    

  if ( rut == null || dv == null )
    return 0    
    var dvr = '0'    
    suma = 0    
    mul  = 2    

    for (i= rut.length -1 ; i >= 0; i--){    
        suma = suma + rut.charAt(i) * mul        
        if (mul == 7)            
            mul = 2        
        else                
            mul++    
    }    
    res = suma % 11    
    if (res==1)        
        dvr = 'k'    
    else if (res==0)        
        dvr = '0'    
    else    
    {        
        dvi = 11-res        
        dvr = dvi + ""    
    }
    if ( dvr != dv.toLowerCase() )    
    {
        
        //alert("EL rut es incorrecto")        
        //document.form1.rut.focus();        
        //document.form1.rut.select();        
        return false    
    }

    return true
}

function Rut(texto){
	//alert(texto);    
  var tmpstr = "";    
  for ( i=0; i < texto.length ; i++ )        
    if ( texto.charAt(i) != ' ' && texto.charAt(i) != '.' && texto.charAt(i) != '-' )
        tmpstr = tmpstr + texto.charAt(i);    
    texto = tmpstr;    
    largo = texto.length;    

    if ( largo < 2 ){
    	/*
        $('#mensajeRut1').removeAttr("hidden");
        $('#mensajeRut2').attr("hidden","hidden");
        */       
        //alert("Debe ingresar el rut completo");
        //document.form1.rut.focus();        
        //document.form1.rut.select();        
        return false;    
    }    

    for (i=0; i < largo ; i++ ){            
        if ( texto.charAt(i) !="0" && texto.charAt(i) != "1" && texto.charAt(i) !="2" && texto.charAt(i) != "3" && texto.charAt(i) != "4" && texto.charAt(i) !="5" && texto.charAt(i) != "6" && texto.charAt(i) != "7" && texto.charAt(i) !="8" && texto.charAt(i) != "9" && texto.charAt(i) !="k" && texto.charAt(i) != "K" ){ 
                     
            //alert("El valor ingresado no corresponde a un rut valido");            
            //document.form1.rut.focus();            
            //document.form1.rut.select();            
            return false;        
        }    
    }    

    var invertido = "";    
    for ( i=(largo-1),j=0; i>=0; i--,j++ )        
        invertido = invertido + texto.charAt(i);    
    var dtexto = "";    
    dtexto = dtexto + invertido.charAt(0);    
    dtexto = dtexto + '-';    
    cnt = 0;    

    for ( i=1,j=2; i<largo; i++,j++ ){        
        //alert("i=[" + i + "] j=[" + j +"]" );        
        if ( cnt == 3 ){            
            dtexto = dtexto + '.';            
            j++;            
            dtexto = dtexto + invertido.charAt(i);            
            cnt = 1;        
        }else{                
           dtexto = dtexto + invertido.charAt(i);            
           cnt++;        
        }    
    }    

    invertido = "";    
    for ( i=(dtexto.length-1),j=0; i>=0; i--,j++ )        
        invertido = invertido + dtexto.charAt(i);    

    //alert(invertido);
    $("#rut").val(invertido.toUpperCase());
    //document.form_pacientes.rut.value = invertido.toUpperCase();      

    if(revisarDigito2(texto)){
        //ACÁ ESTA TODO BUENO, si se llega hasta acá
        
        return true; 
    }else{
        return false;
    }   
     
}
/*fin funciones para rut*/


/*inicio verificar porcentaje odontologo*/
function verificarFonoContacto(valor)
{
	//alert(valor.length);
	if(valor.length >= 9){
		//alert(valor.length);
		toastr.info("El número no puede superar los 9 digitos", "Ayuda", options);
	}
	return false;
}
/*fin verificar porcentaje odontologo*/

</script>

@endpush