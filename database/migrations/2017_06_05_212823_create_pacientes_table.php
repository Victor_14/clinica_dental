<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('apellido');
            $table->string('rut',15)->unique();
            $table->string('correo')->unique()->nullable();
            $table->integer('edad');
            $table->string('direccion')->nullable();
            $table->string('prevision');
            $table->date('fecha_nacimiento')->nullable();
            $table->integer('telefono_contacto')->nullable();
            $table->integer('config')->comment('si es 0, no se puede borrar ni editar');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
