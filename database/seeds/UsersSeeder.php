<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert(array(
              array(
              	'id' => '1',
              	'nombre' => 'VICTOR',
              	'apellido' => 'DESARROLLO',
              	'email' => 'VICTOR.IBARRAO@UTEM.CL',
              	'rut' => '000000000',
              	'tipo_usuario_id' => '1',//admin
              	'password' => bcrypt('pepe'),
              	'config' => '0',//no se puede borrar ni editar
                'porcentaje' => NULL,//para odontologo
                'telefono_contacto' => NULL,
              	'remember_token' => NULL,
              	'created_at' => '2017-05-29 17:16:01',
              	'updated_at' => '2017-05-29 17:16:02'),
              array(
              	'id' => '2',
              	'nombre' => 'FELIPE',
              	'apellido' => 'ALARCON',
              	'email' => 'PIPEEE.ALARCON@GMAIL.COM',
              	'rut' => '183257919',
              	'tipo_usuario_id' => '2',//admin odontologo
              	'password' => bcrypt('pepe'),
              	'config' => '0',//no se puede borrar ni editar
                'porcentaje' => "40",//para odontologo
                'telefono_contacto' => NULL,
              	'remember_token' => NULL,
              	'created_at' => '2017-05-29 17:16:01',
              	'updated_at' => '2017-05-29 17:16:02'),
              array(
                'id' => '3',
                'nombre' => 'JUANITO',
                'apellido' => 'PEREZ',
                'email' => 'PRUEBA_ODONTOLOGO1@GMAIL.Com',
                'rut' => '222222222',
                'tipo_usuario_id' => '3',//odontologo
                'password' => bcrypt('pepe'),
                'config' => '1',//si se puede borrar y editar
                'porcentaje' => "28",//para odontologo
                'telefono_contacto' => NULL,
                'remember_token' => NULL,
                'created_at' => '2017-05-29 17:16:01',
                'updated_at' => '2017-05-29 17:16:02'),
              array(
                'id' => '4',
                'nombre' => 'MARIA',
                'apellido' => 'JACOBE',
                'email' => 'PRUEBA_RECEPCION@GMAIL.COM',
                'rut' => '333333333',
                'tipo_usuario_id' => '4',//recepsionista
                'password' => bcrypt('pepe'),
                'config' => '1',//si se puede borrar ni editar
                'porcentaje' => NULL,//para odontologo
                'telefono_contacto' => NULL,
                'remember_token' => NULL,
                'created_at' => '2017-05-29 17:16:01',
                'updated_at' => '2017-05-29 17:16:02'),
              array(
                'id' => '6',
                'nombre' => 'CONEJO',
                'apellido' => 'DELOSDIENTES',
                'email' => 'PRUEBA_ODONTOLOGO3@GMAIL.COM',
                'rut' => '444444444',
                'tipo_usuario_id' => '3',//odontologo
                'password' => bcrypt('pepe'),
                'config' => '1',//si se puede borrar ni editar
                'porcentaje' => '39',//para odontologo
                'telefono_contacto' => NULL,
                'remember_token' => NULL,
                'created_at' => '2017-05-29 17:16:01',
                'updated_at' => '2017-05-29 17:16:02'),
        ));

        $users = App\User::where('id',1)->get();//Administrador
        $role = App\Role::where('name', '=', 'ADMINISTRADOR')->get()->first();
        foreach ($users as $user) {
          $user->attachRole($role);  
        }

        $users = App\User::where('id',2)->get();//Administrador odontologo
        $role = App\Role::where('name', '=', 'ADMINISTRADOR ODONTÓLOGO')->get()->first();
        foreach ($users as $user) {
          $user->attachRole($role);  
        }
        

        $users = App\User::where('id',6)->orwhere('id',3)->get();//Odontologos
        $role = App\Role::where('name', '=', 'ODONTÓLOGO')->get()->first();
        foreach ($users as $user) {
          $user->attachRole($role);
        }

        $user = App\User::findorfail(4);//recepcionista
        $role = App\Role::where('name', '=', 'RECEPCIONISTA')->get()->first();
        $user->attachRole($role);
        
    }
}
