<?php

use Illuminate\Database\Seeder;

class TipoUsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_usuarios')->delete();

        DB::table('tipo_usuarios')->insert(array(
              array(
              	'id' => '1',
              	'nombre' => 'ADMINISTRADOR',
              	'created_at' => '2017-05-29 14:16:01',
              	'updated_at' => '2017-05-29 14:16:02'),
              array(
              	'id' => '2',
              	'nombre' => 'ADMINISTRADOR ODONTÓLOGO',
              	'created_at' => '2017-05-29 14:16:01',
              	'updated_at' => '2017-05-29 14:16:02'),
              array(
                'id' => '3',
                'nombre' => 'ODONTÓLOGO',
                'created_at' => '2017-05-29 14:16:01',
                'updated_at' => '2017-05-29 14:16:02'),
              array(
                'id' => '4',
                'nombre' => 'RECEPCIONISTA',
                'created_at' => '2017-05-29 14:16:01',
                'updated_at' => '2017-05-29 14:16:02'),
        ));
    }
}
