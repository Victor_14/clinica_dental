<?php

use Illuminate\Database\Seeder;

class EnfermedadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('enfermedades')->delete();

        DB::table('enfermedades')->insert(array(
              array(
              	'id' => '1',
              	'nombre' => 'HIPERTENSIÓN',
              	'config' => '0',
              	'created_at' => '2017-05-29 14:16:01',
              	'updated_at' => '2017-05-29 14:16:02'),
              array(
              	'id' => '2',
              	'nombre' => 'DIABETES',
              	'config' => '0',
              	'created_at' => '2017-05-29 14:16:01',
              	'updated_at' => '2017-05-29 14:16:02'),
              array(
                'id' => '3',
                'nombre' => 'ENFERMEDAD ENDOCRINICA (HIPO/HIPERTIROIDISMO)',
                'config' => '0',
                'created_at' => '2017-05-29 14:16:01',
                'updated_at' => '2017-05-29 14:16:02'),
              array(
                'id' => '4',
                'nombre' => 'EPILELSIA',
                'config' => '0',
                'created_at' => '2017-05-29 14:16:01',
                'updated_at' => '2017-05-29 14:16:02'),
              array(
                'id' => '5',
                'nombre' => 'ALTERACIÓN NEUROLÓGICA',
                'config' => '0',
                'created_at' => '2017-05-29 14:16:01',
                'updated_at' => '2017-05-29 14:16:02'),
              array(
                'id' => '6',
                'nombre' => 'CANCER',
                'config' => '0',
                'created_at' => '2017-05-29 14:16:01',
                'updated_at' => '2017-05-29 14:16:02'),
              array(
                'id' => '7',
                'nombre' => 'VIH / SIDA',
                'config' => '0',
                'created_at' => '2017-05-29 14:16:01',
                'updated_at' => '2017-05-29 14:16:02'),
              array(
                'id' => '8',
                'nombre' => 'OTROS',
                'config' => '1',
                'created_at' => '2017-05-29 14:16:01',
                'updated_at' => '2017-05-29 14:16:02'),
        ));
    }
}
