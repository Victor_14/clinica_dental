<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        $admin = Role::create(array(
                'name' => 'ADMINISTRADOR'//todos los permisos siempre
        ));

        $adminOdontologo = Role::create(array(
                'name' => 'ADMINISTRADOR ODONTÓLOGO'//todos los permisos siempre
        ));

        $odontologo = Role::create(array(
                'name' => 'ODONTÓLOGO'
        ));

        $recepsionista = Role::create(array(
                'name' => 'RECEPCIONISTA'
        ));

        $permisos = Permission::all();
        foreach($permisos as $permiso){
           $admin->attachPermission($permiso);
           $adminOdontologo->attachPermission($permiso);
        }

        $permisos = Permission::where('name','ver_usuarios')
        						->Orwhere('name','ver_roles')
        						->Orwhere('name','ver_pacientes')
        						->Orwhere('name','editar_pacientes')
        						->Orwhere('name','eliminar_pacientes')
        						->Orwhere('name','crear_pacientes')
        						->Orwhere('name','ver_enfermedades')
        						->get();
        foreach($permisos as $permiso){
           $odontologo->attachPermission($permiso);
        }

        $permisos = Permission::where('name','ver_usuarios')
        						->Orwhere('name','ver_roles')
        						->Orwhere('name','ver_pacientes')
        						->Orwhere('name','ver_enfermedades')
        						->get();
        foreach($permisos as $permiso){
           $recepsionista->attachPermission($permiso);
        }

    }
}
