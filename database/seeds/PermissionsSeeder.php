<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('permissions')->delete();
        $ver_usuarios = new Permission();
	    $ver_usuarios->name = 'ver_usuarios';
	    $ver_usuarios->display_name = 'ver usuarios';
	    $ver_usuarios->save();
	    
	    $ver_roles = new Permission();
	    $ver_roles->name = 'ver_roles';
	    $ver_roles->display_name = 'ver roles';
	    $ver_roles->save();

	    $ver_pacientes = new Permission();
	    $ver_pacientes->name = 'ver_pacientes';
	    $ver_pacientes->display_name = 'ver pacientes';
	    $ver_pacientes->save();

	    $ver_enfermedades = new Permission();
	    $ver_enfermedades->name = 'ver_enfermedades';
	    $ver_enfermedades->display_name = 'ver enfermedades';
	    $ver_enfermedades->save();
	    
	    $crear_roles = new Permission();
	    $crear_roles->name = 'crear_roles';
	    $crear_roles->display_name = 'crear roles';
	    $crear_roles->save();
	    
	    $crear_usuarios = new Permission();
	    $crear_usuarios->name = 'crear_usuarios';
	    $crear_usuarios->display_name = 'crear usuarios';
	    $crear_usuarios->save();

	    $crear_pacientes = new Permission();
	    $crear_pacientes->name = 'crear_pacientes';
	    $crear_pacientes->display_name = 'crear pacientes';
	    $crear_pacientes->save();

	    $crear_enfermedades = new Permission();
	    $crear_enfermedades->name = 'crear_enfermedades';
	    $crear_enfermedades->display_name = 'crear enfermedades';
	    $crear_enfermedades->save();
	    
	    $editar_roles = new Permission();
	    $editar_roles->name = 'editar_roles';
	    $editar_roles->display_name = 'editar roles';
	    $editar_roles->save();
	    
	    $editar_usuarios = new Permission();
	    $editar_usuarios->name = 'editar_usuarios';
	    $editar_usuarios->display_name = 'editar usuarios';
	    $editar_usuarios->save();

	    $editar_pacientes = new Permission();
	    $editar_pacientes->name = 'editar_pacientes';
	    $editar_pacientes->display_name = 'editar pacientes';
	    $editar_pacientes->save();

	    $editar_enfermedades = new Permission();
	    $editar_enfermedades->name = 'editar_enfermedades';
	    $editar_enfermedades->display_name = 'editar enfermedades';
	    $editar_enfermedades->save();
	    
	    $eliminar_usuarios = new Permission();
	    $eliminar_usuarios->name = 'eliminar_usuarios';
	    $eliminar_usuarios->display_name = 'eliminar usuarios';
	    $eliminar_usuarios->save();
	    
	    $eliminar_roles = new Permission();
	    $eliminar_roles->name = 'eliminar_roles';
	    $eliminar_roles->display_name = 'eliminar roles';
	    $eliminar_roles->save();

	    $eliminar_pacientes = new Permission();
	    $eliminar_pacientes->name = 'eliminar_pacientes';
	    $eliminar_pacientes->display_name = 'eliminar pacientes';
	    $eliminar_pacientes->save();

	    $eliminar_enfermedades = new Permission();
	    $eliminar_enfermedades->name = 'eliminar_enfermedades';
	    $eliminar_enfermedades->display_name = 'eliminar enfermedades';
	    $eliminar_enfermedades->save();
    }
}
